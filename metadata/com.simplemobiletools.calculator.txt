Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-Calculator
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Calculator/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Calculator/blob/HEAD/CHANGELOG.md

Auto Name:Simple Calculator
Summary:A basic calculator with a customizable widget
Description:
A simple calculator with the basic operations. Long press the result or formula
to copy the value to clipboard. The text color of the widget can be customized,
as well as the color and the alpha of the background. Press the result or
formula in the widget to open the app.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Calculator

Build:1.6,6
    commit=3baa0952abb9dd6b14aaa8540d3209de2bc4a68e
    subdir=app
    gradle=yes

Build:1.7,7
    commit=1.7
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.7
Current Version Code:7
